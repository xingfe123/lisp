
(ql:quickload :drakma)

;; curl -s --request POST --url http://10.97.25.221:5555/v1.0/ports  --header 'cache-control: no-cache' --header 'content-type: application/json' -d @/tmp/d.json`
;(defclass

;;       {
;;     "port": {
;;         "description": "test",
;;         "host_id": "$1",
;;         "name": "test",
;;         "network_id": "qatestnetwork",
;;         "security_groups": [],
;;         "subnet_id": "qatestsubnet",
;;         "tenant_id": "qatesttenant"
;;     }
;; }

(defclass json-empty-list ()
  ())

(defmethod json:encode-json ((object json-empty-list) &optional stream)
  (princ "[]" stream)
  nil)

(defvar *json-empty-list* (make-instance 'json-empty-list))


(defun json-bool-handler (token)
  (or (string= token "true")
      (and (string= token "false") *json-false*)))

(defmacro preserving-json-boolean (opts &body body)
  (declare (ignore opts))
  `(let ((json:*boolean-handler* #'json-bool-handler))
     ,@body))



(defclass port-info()
  ((description :initarg :description :initform "test" :reader description)
   (host-id :initarg :host-id :initform "")
   (name :initarg :name :initform "test")
   (network-id :initarg :network-id :initform "qatestnetwork")
   (security-groups :initarg :security-groups :initform *json-empty-list*)
   (subnet-id :initarg :subnet-id :initform "qatestsubnet")
   (tenant-id :initarg :tenant-id :initform "qatesttenant")
   ))
(defmethod json:encode-json ((port-info port-info) &optional stream)
  (with-slots (description name host-id tenant-id subnet-id network-id) port-info
    (princ "{" stream)
    (json:encode-json "description" stream)
    (princ ":" stream)
    (json:encode-json description stream)
    (princ "," stream)
    
    (json:encode-json "name" stream)
    (princ ":" stream)
    (json:encode-json name stream)
    (princ "," stream)

    (json:encode-json "host_id" stream)
    (princ ":" stream)
    (json:encode-json host-id stream)
    (princ "," stream)
    
    (json:encode-json "tenant_id" stream)
    (princ ":" stream)
    (json:encode-json tenant-id stream)
    (princ "," stream)
    
    (json:encode-json "network_id" stream)
    (princ ":" stream)
    (json:encode-json network-id stream)

    (princ "," stream)
    
    (json:encode-json "subnet_id" stream)
    (princ ":" stream)
    (json:encode-json subnet-id stream)

; tenant_id, network_id, subnet_id
    (princ "}" stream))
  nil)

;; 10.97.25.221
;; 10.97.25.223
;; 10.97.25.205
(defun make-post-info(host-id)
  (let ((port-info (make-hash-table)))
    (setf (gethash :port port-info) (make-instance 'port-info :host-id host-id))
    (json:encode-json-to-string port-info)))

(defvar *port-set* ())
(defun create-port()
  (let ((recv (json:decode-json (drakma:http-request "http://10.97.25.221:5555/v1.0/ports"
					 :method :post
					 :content-type "application/json"
					 :content (make-post-info "10.97.25.221")
					 :want-stream t))))
    (pushnew  (cdadr (assoc :port recv))
	      *port-set*)))


(defun delete-port(port-id)
  )

(defun clear-port()
  (dolist (port *port-set*)
    (format T "~a~%" port)))





		     
