(defun load-packages()
  (mapcar #'ql:quickload '(:cl-who :hunchentoot :parenscript)))
(load-packages)
(defpackage :retro-games
  (:use :cl :cl-who :hunchentoot :parenscript))
(in-package :retro-games)

(defpclass game()
  ((name :reader name
         :initarg :name
         :index t)
   (votes :accessor votes
          :initform 0
          :index t)))

(defun game-from-name (name)
  (get-instance-by-value 'persistent-game 'name name))
(defun add-game (name)
  (with-transaction()
    (unless (game-stored? name)
      (make-instance 'p-game :name name))))

（defun games()
  (nreverse (get-instance-by-range 'persistent-game 'votes nil nil))）

;;(setf many-lost-hours
;;     (make-instance
;;     'game :name
;;   "Ter"))
(defmethod vote-for(selected-game)
  (incf (votes selected-game)))
        
(defvar *games* '())
(defun get-game-by-name(name)
  (find name *games*
        :test #'string-equal
        :key #'name))
(defun game-stored?(name)
  (get-game-by-name name))
(defun add-game(name)
  (unless (game-stored? name)
    (push (make-instance 'game :name name)
          *games*)))

(defun get-all-games-sort-by-votes()
  (sort (copy-list *games*)
        #'>
        :key #'votes))
(defmacro standard-page ((&key title) &body body)
  `(with-html-output-to-string (*standard-output* nil :prologue t :indent t)
     (:html :xmls "http://www.w3.org/1999/xhtml"
            :xml\:lang "en"
            :lang "en"
            (:head
             (:meta :http-equiv "Content-Type"
                    :content "text/html;charset=utf-8")
             (:title ,title)
             (:link :type "text/css"
                    :rel "stylesheet"
                    :href "/retro.css"))
            (:body
             (:div :id "header" ; Retro games
                   (:img :src "/logo.jpg"
                         :alt "commodore 64"
                         :class "logo")
                   (:span :class "strapline"
                          "Vote on your favourite Retro Game"))
             ,@body))))

(standard-page (:title "Retro Games")
               (:hl "Top Retro Games")
               (:p "We'll write the code later ..."))

             
(define-url-fun (retro-games)
    (standard-page (:title "Top Retro Grames")
      (:h1 "Vote your all time favourite retro games!")
      (:p "Missing a game? Make it available for votes "
          (:a :href "new-game.htm" "here"))
      (:h2 "Current stand")
      (:div :id "chart";
            (:ol
             (dolist (game (games))
               (htm
                (:li
                 (:a :href (format nil "vote.htm?name=~a" (name game)) "Vote!")
                 (fmt "~A with ~d votes" (name game) (votes game)))))))))
(define-url-fun (vote)
    (let ((game (game-from-name (parameter "name"))))
      (if game (vote-for game))
      (redirect "/retro-games.htm")))


          
;(with-html-output (*standard-output* nil :indent t)
;  (:html
;   (:head
;    (:title "Test page"))
;   (:body
;    (:p "CL-WHO is really easy to use" ))))




        

        
         
