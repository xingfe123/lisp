
(defgeneric current-song(source)
  (:documentation "return the currently palying song or NIL"))
(defgeneric maybe-move-to-next-song(song source)
  (:documentation "If the given song is still the current one update the value returned by current-song"))
(defgeneric still-current-p(song source)
  (:documentation "return true if the song given is the same as the current-song"))

(defclass song()
  ((file :reader file :initarg :file)
   (title :reader title :initarg :title)
   (id3-size :reader id3-size :initarg :id3-size)))
(defgeneric find-song-source (type request)
  (:documentation "Find the song-type of the given type for the given request."))

(defclass simple-song-queue()
  ((songs :accessor songs :initform (make-array 10 :adjustable t :fill-pointer 0))
   (index :accessor index :initform 0)))

(defparameter *songs* (make-instance 'simple-song-queue))
(defmethod find-song-source ((type (eql 'singleton)) request)
  (declare (ignore request))
  *songs*)

(defmethod current-song ((source simple-song-queue))
  (when (array-in-bounds-p (songs source) (index source))
    (aref (songs source) (index source))))

(defmethod still-current-p (song (source simple-song-queue))
  (eql song (current-song source)))

(defmethod maybe-move-to-next-song (song (source simple-song-queue))
  (when (still-current-p song source)
    (incf (index source))))

(defun add-file-to-song(file)
  (vector-push-extend (file->song file) (songs *songs*)))

(defun file->song(file)
  (let ((id3 (read-id3 file)))
    (make-instance
     'song
     :file (namestring (truename file))
     :title (format nil "" (song id3)
		    (artist id3)
		    (album id3))
     :id3-size (size id3))))



  
(defun shoutcast (request entity)
  (with-http-response
      (request entity :content-type "audio/mp3" :timeout *timeout-seconds*)
    (prepare-icy-response request *metadata-interval*)
    (let ((wants-metadata-p (header-slot-value request :icy-metadata)))
      (with-http-body (request entity)
	(play-songs
	 (request-socket request)
	 (find-song-source *song-source-type* request)
	 (if wants-metadata-p *metadata-interval*))))))
(defun prepare-icy-response (request metadata-interval)
  (setf (request-reply-protocol-string request) "icy")
  (loop for (k v) in (reverse `())
     do (setf (reply-header-slot-value request k) v))
  (turn-off-chunked-transfer-encoding request))

(defun turn-off-chunked-transfer-encoding(request)
  (setf (request-reply-strategy request)
	(remove :chunked (request-reply-strategy request))))

(defparameter *metadata-interval* (expt 2 12))
(defparameter *song-source-type* 'singleton)
(defun paly-songs (stream song-source metadata-interval)
  (handler-case
      (loop for next-metadata = metadata-interval
	 then (play-current
	       stream
	       song-source
	       next-metadata
	       metadata-interval)
	 while next-metadata)
    (error (e) (format *trace-output* "caught error in play-songs:~a" e))))

(defun paly-current(out song-source next-metadata metadata-interval)
  (let ((song (current-song song-source)))
    (when song)))
(defun make-icy-metadata(title)
  (let* ((text (format nil "StreamTitle='~a';" (substitute #\Space #' title)))
	 (blocks (ceiling (length text) 16))
	 (buffer (make-array (1+ (* blocks 16))
			     :element-type '(unsigned-byte 8)
			     :initial-element 0)))
    (setf (aref buffer 0) blocks)
    (loop for char accros text
       for i from 1
       do (setf (aref buffer i) (char-code char)))
    buffer))

(defun play-current (out song-source next-metadata metadata-interval)
  (let ((song (current-song song-source)))
    (when song )))


	
  

