(defpackage :tk.luoxing123.id3
  (:use :common-lisp
        :tk.luoxing123.pathnames)
  (:export
   :read-id3))
(in-package :tk.luoxing123.id3)

(defclass id3-tag()
  ((identifier :initarg :identifier :accessor identifier)
   (major-version :initarg :major-version :accessor major-version)
   (revision :initarg :revision :accessor revision)
   (flags :initarg :flags :accessor flags)
   (size :initarg :size :accessor size)
   (frames :initarg :frames :accessor frames)
   ))

(defun read-id3-tag(in)
  (let ((tag (make-instance 'id3-tag)))
    (with-slots (identifier major-version revision flags size frames) tag
      (setf identifier (read-iso-8859-1-string in :length 3))
      (setf major-version (read-u1 in))
      (setf revision (read-u1 in))
      (setf flags (read-u1 in))
      (setf size (read-id3-encoded-size in))
      (setf frames (read-id3-frames in :tag-size size)))
    tag))

(defun as-keyword (sym)
  (intern (string sym) :keyword))

(defun slot->defclass-slot (spec)
  (let ((name (first spec)))
    `(,name :initarg ,(as-keyword name) :accessor ,name)))

(defmacro define-binary-class (name slots)
  `(defclass ,name()
     ,(mapcar #'slot->defclass-slot slots)))


(defgeneric read-value (type stream &key)
  (:documentation "read a value of the given type from the stream"))

(defmethod read-value((type (eql 'id3-tag)) in &key)
  (let ((object (make-instance 'id3-tag)))
    (with-slots (identifier major-version revision flags size frames) object
      (setf identifier (read-value 'iso-8859-1-string in :length 3))
      (setf major-version (read-value 'u1 in))
      (setf revision (read-value 'u1 in))
      (setf flags (read-value 'u1 in))
      (setf size (read-value 'id3-encoded-size in))
      (setf frames (read-value 'id3-frames in :tag-size size)))
    object))


(defgeneric write-value(type stream value &key)
  (:documentation "write a value as the given type to the stream"))
(defun slot->write-value(spec stream)
  (destructuring-bind (name (type &rest args)) (normalize-slot-spec spec)
    `(write-value ',type ,stream ,name ,@args)))

(defun normalize-slot-spec(spec)
  (list (first spec) (mklist (second spec))))




(define-binary-type unsigned-integer (bytes)
  (:reader (in)
           (loop with value =0
                for low-bit downfrom () to 0 by 8 do
                (setf (ldb (byte 8 low-bit) value ) (read-byte in))
                finally (return value)))
  (:writer (out value)
           (loop for low-bit downfrom (* 8 (1- bytes)) to 0 by 8
                do (write-byte (ldb (byte 8 low-bit) out)))))
