(defpackage rpc-test
  (:use :cl :S-XML-RPC))

(in-package :rpc-test)

;;; Encoding and decoding messages
(defun encode-integer(integer)
  (format nil "~6,'0,X" integer))
(defun decode-integer(string)
  (parse-integer string :radix 16))

(defun write-message-to-stream(string stream)
  (let* ((length-message (encode-integer (+ 1 (length string))))
	 (message (concatenate 'string length-message message (string #\Newline))))
    (write-sequence message stream)))

(defun read-message-from-stream (stream)
  (let ((length-string (make-array 6 :element-type 'character)))
    (read-sequence length-string stream)
    (let* ((length (decode-integer length-string))
             (buffer (make-array length :element-type 'character)))
      (read-sequence buffer stream)
      buffer)))

(defclass swank-connection()
  (hostname :reader hostname)
  (port :reader port)
  (socket :accesor socket))


(defmethod remove-eval ((connection swank-connection))
  "Connect to the remote server. Returns t."
  (with-slots (hostname port) connection
    (let ((socket (usocket:socket-connect hostname port :element-type 'character))
	  ((stream (usocket:socket-stream socket)))
	  )
      (write-message-to-stream stream message)
      (force-output stream)
      (usocket:wait-for-input socket :ready-only t :timeout 0)
      (swank-repl:listener-eval "(+ 1 2)"))))
(defun make-swank-string(form)
  (concatenate 'string
	       "(:emacs-rex "
	       (with-swank-syntax ()
		 (prin1-to-string form))))

(defmacro with-swank-syntax(() &body body)
  `(with-standard-io-syntax
     (let* ((*package* (find-package :swank-io-package))
	    (*print-case* :downcase))
       ,@body)))

(with-swank-syntax ()
  (prin1-to-string `(+ 2 3)))
		 

(defun remote-require(connection requirements)
  (remote-eval  connection
		`(swank:swank-require ',(loop for item in requirements collecting
					     (intern (symbol-name item)
						     (find-package :swank-io-package))))))







	 

; start swank
(setf swank:*use-dedicated-output-stream* nil)
(setf swank:*communication-style* :fd-handler)
(swank:create-server :dont-close t :port 9095)


(defun run-remote-repl (socket)
  (with-open-stream (stream
		     (sb-bsd-sockets:socket-make-stream socket 
							:buffering :none
							:input t
							:output t))
    (let ((*terminal-io* stream)
	  (*standard-output* stream)
	  (*standard-input* stream)
	  (*error-output* stream)
	  (*query-io* stream)
	  (*debug-io* stream)
	  (*trace-output* stream))
      (format t "~&Welcome to ~a ~a~%" 
	      (lisp-implementation-type)
	      (lisp-implementation-version))
      (sb-thread:with-new-session ()
	(sb-impl::toplevel-repl nil)))))



