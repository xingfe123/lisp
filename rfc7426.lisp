;; Software-Defined Networking (SDN) - A programmable networks
;; approach that supports the separation of control and forwarding
;; planes via standardized interfaces.

;; Resource - A physical or virtual component available within a
;; system.  Resources can be very simple or fine-grained (e.g., a
;; port or a queue) or complex, comprised of multiple resources
;; (e.g., a network device).

;; Network Device - A device that performs one or more network
;; operations related to packet manipulation and forwarding.  This
;; reference model makes no distinction whether a network device is
;; physical or virtual.  A device can also be considered as a
;; container for resources and can be a resource in itself.

;; Interface - A point of interaction between two entities.  When the
;;       entities are placed at different locations, the interface is
;;       usually implemented through a network protocol.  If the entities
;;       are collocated in the same physical location, the interface can be
;;       implemented using a software application programming interface
;;       (API), inter-process communication (IPC), or a network protocol.


;; Application (App) - An application in the context of SDN is a
;;       piece of software that utilizes underlying services to perform a
;;       function.  Application operation can be parameterized, for
;;       example, by passing certain arguments at call time, but it is
;;       meant to be a standalone piece of software; an App does not offer
;;       any interfaces to other applications or services.

;; Service - A piece of software that performs one or more functions
;;       and provides one or more APIs to applications or other services of
;;       the same or different layers to make use of said functions and
;;       returns one or more results.  Services can be combined with other
;;       services, or called in a certain serialized manner, to create a
;;       new service.


;; Forwarding Plane (FP) - The collection of resources across all
;;       network devices responsible for forwarding traffic.

;; Operational Plane (OP) - The collection of resources responsible
;;       for managing the overall operation of individual network devices.

;;   Control Plane (CP) - The collection of functions responsible for
;;       controlling one or more network devices.  CP instructs network
;;       devices with respect to how to process and forward packets.  The
;;       control plane interacts primarily with the forwarding plane and,
;;       to a lesser extent, with the operational plane.

;;   Management Plane (MP) - The collection of functions responsible
;;       for monitoring, configuring, and maintaining one or more network
;;       devices or parts of network devices.  The management plane is
;;       mostly related to the operational plane (it is related less to the
;;       forwarding plane).

;;   Application Plane - The collection of applications and services
;;       that program network behavior.


;; Device and resource Abstraction Layer (DAL) - The device's
;;       resource abstraction layer based on one or more models.  If it is
;;       a physical device, it may be referred to as the Hardware
;;       Abstraction Layer (HAL).  DAL provides a uniform point of
;;       reference for the device's forwarding- and operational-plane
;; resources.


;;  Control Abstraction Layer (CAL) - The control plane's abstraction
;;       layer.  CAL provides access to the Control-Plane Southbound
;; Interface.

;; Management Abstraction Layer (MAL) - The management plane's
;;       abstraction layer.  MAL provides access to the Management-Plane
;; Southbound Interface.

;;  Network Services Abstraction Layer (NSAL) - Provides service
;;       abstractions that can be used by applications and services.


