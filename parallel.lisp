(defpackage :example (:use :cl :lparallel))
(in-package :example)

(defun fib (n)
  (if (< n 2)
      n
      (let ((a (fib (- n 1)))
            (b (fib (- n 2))))
        (+ a b))))
(setf lparallel:*kernel* (lparallel:make-kernel 2))

(defun pfib-slow (n)
  (if (< n 2)
      n
      (plet ((a (pfib-slow (- n 1)))
             (b (pfib-slow (- n 2))))
        (+ a b))))

