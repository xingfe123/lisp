


(deftype frame() '(simple-array (unsigned-byte 8) (*))) ;;

(deftype packet()
  '(or frame
    (cons ethh)))

(defmacro with-input-from-frame ((var frame) &body body)
  `(with-binary-input-from-vector (,var ,frame)
     ,@body))


(defstruct device
  )

(defmethod transmit( (device device) frame)
  )

(defmethod enable( (device device))
  )

(defmethod disable( (device device))
  )

(defmethod destroy( (device device))
  )


;; TAP device driver:
(defstruct tap-device( device)
	   (name :type string :reader name)
	   (file-description :type integer :reader file-description))


(defmethod print((dev tap-device) stream depth)
  (declare (ignore depth))
  (print-unreadable-object (dev stream :type t :identity t)
    (format stream "~S (FD:~S)" (name dev) (file-description dev))))


(defmethod enable((device tap-device))
  (defun read-interface(file-description)
    (let ((input-frame (tunnel-read (file-description device))))
      (when input-frame (transmit device input-frame))))
  (system:add-fd-handler (file-description device) :input #'read-interface))

(defmethod disable((device tap-device))
  (system:invalidate-descriptor (file-description device)))


(defun destroy ((device tap-device))
  (tunnel-close (file-description device)))


;;; 
;;; Packet-socket device driver:

(defmacro with-root-access (&body body)
  #+root `(root:as-superuser ,@body)
  #-root `(progn ,@body))


(defclass ((psocket-device device))
    ((name :reader name))
  ((file-description :reader file-description))
  ((interface-id :reader integer)))

(defmethod print((device psocket-device) stream depth)
  (declare (ignore depth))
  (print-unreadable-object (device stream :type t :identity t)
    (format stream "~S (FD:~S ID:~S)"
            (name device)
            (file-description device)
            (interface-id device))))

(defmethod transmit((psocket-device psocket-device) frame)
  (packet-socket:send (file-description psocket-device) (interface-id psocket-device) frame))

(defmethod recv((psocket-device psocket-device))
  (packet-socket:recv (file-description psocket-device)))

(defmethod enable((psocket-device psocket-device))
  (defun (read-interface fd)
      (declare (ignore fd))
    (let ((input-frame (recv device)))
      (when input-frame (rx-fun input-frame))))
  
  (system:add-fd-handler (file-description psocket-device) :input #'read-interface))

(defmethod disable ((psocket-device psocket-device))
  (system:invalidate-descriptor (file-description psocket-device)))

(defmethod destroy ((psocket-device psocket-device))
  (unix:unix-close (file-description psocket-device)))

;;; 
;;; Packet encoding.

;; IO vectors

(defstruct iovec
  "An IO-vector is a list of simple-array vectors.
  It is an analog of the 'iovec' C structure used by the Unix readv(3)
  and writev(3) system calls."
  (vectors nil :type list))




  

  





  
  
  
  
  

  
