
(defclass action ()
  "actions is associated with each packet, is carried between flow tables
when no contain a goto-table instruction, pipeline processing stops and action the sections"
  (action-type :reader action-type :type action-type)
  )
(deftype apply-action-type () ;; in the order specified below
  '(member :copy-ttl-inwards
    :pop :push-mpls :push-pbb :push-vlan :copy-ttl-outwards
    :decrement-ttl :set :qos :group :output))

(deftype action-type ()
  '(member :output
    :set-queue
    :drop
    :group
    :push-tag
    :pop-tag ;; valn mpls pbb
    :set-field ;;
    :change-ttl ;; mpls ip copy-ttl-outwards copy-ttl-inwards
    ))


(defclass packet ()
  ((actions :accessor actions :initform nil)))

(defmethod apply-action ((packet packet) )
  )



(defun drop (packet)
  ;;todo 
  )
(defclass open-switch()
  ((flow-tables :initform nil :reader flow-tables)
   (group-table :initform nil :reader group-table)
   (ports :initform nil :reader ports)
   (miss-flow-entry :reader miss-flow-entry)))

(defmethod miss-flow-entry-exist? ((os open-switch))
  (miss-flow-entry os))

(defmethod action ((os open-switch) (packet packet))
  (let* ((table0 (find-if #'zerop-flow-table (flow-tables os)))
	 (instruction (match table0 packet)))
    (if instruction (action  instruction packet)
	(if (miss-flow-entry-exist? os)
	    (action (miss-flow-entry os) packet)
	    (drop packet)))))


(defclass instruction ()
  ( (actions :reader actions)
    (goto-table :reader goto-table)
    (table-number :reader table-number)))
(defmethod action-set-instruction? ((instruction instruction) )
  ;todo
  )
(defmethod packet-instruction? ((instruction instruction))
  ;todo
  )
(defmethod meta-data-instruction? ((instruction instruction))
  ;todo
  )
(defmethod goto-table-instruction? ((instruction instruction))
  ;; Indicates the next table in the processing pipeline. The table-id
  ;; must be greater than the current table-id. The flow entries of the
  ;; last table of the pipeline can not include this instruction (see
  ;; 5.1). OpenFlow switches with only a single flow table are not
  ;; required to implement this instruction.
  
  )
(defmethod meter-instruction? ((instruction instruction))
  ;todo
  )
(defmethod apply-action-instruction? ((instruction instruction) )
  ;todo
  )
(defmethod clear-action-instrcution? ((instruction instruction))
  ;todo
  )
(defmethod write-action-instruction? ((instruction instruction))
  ;; merge the specified actions into the current action set
  )
(defmethod write-metadata-instruction? ((instruction instruction))
  ;; Writes the masked metadata value into the metadata field. The
  ;; mask specifies which bits of the metadata register should be
  ;; modified (i.e. new metadata = old metadata & ˜mask | value & mask).
  )

;; Meter instruction before Apply-Actions instruction
;; Clear-Actions before Write-Actions
;; Goto-Table is executed last



(defmethod action ((instruction instruction) (packet packet) )
  "todo"
  (defun -update-action-set ()
    (if (action-set-instruction? instruction)
					;todo
	nil 
	)
    (-update-packet))
  (defun -update-packet ()
    (if (packet-instruction? instruction)
					;todo
	nil
	)
    (-update-medata))
  (defun -update-medata ()
    (if (meta-data-instruction? instruction)
					;todo
	nil
	)
    (-goto-table))
  (defun -goto-table ()
    (if (and (goto-instruction? instruction) (table-number instruction)) 
      (action (match (goto-table instruction) packet)
	      packet)))
  (-update-action-set))

(defmethod lookup((os open-switch) (packet packet) )

(defmethod forwarding((os open-switch) packet)
  ;; todo
  )

;;;;;;;;;;;;;;;;;;
(defclass flow-table ()
  ((table-miss :initform nil :reader table-miss)
   (flow-entries :initform nil :accessor flow-entries)
   (table-number :initform 0 :reader table-number
		 :documentation "the packet is first matched against flow entries of flow table 0" )))
(defclass table-miss-entry (flow-entry)
  ()
  (:documentation ""))
(defmethod match? ((table-miss-entry table-miss-entry) (packet packet))
  T)
(defmethod priority ((table-miss-entry table-miss-entry))
  0)
(defmethod action ((table-miss-entry table-miss-entry) (packet packet))
  (setf (reason packet) :table-miss)
  ;todo
  )


(defmethod send-packet-port ((port openflow-port) (packet packet))
  ;todo
  )

(defmethod send-packet-controller ((packet packet))
  (send-packet-port *controller-port* packet))

(defmethod zerop-flow-table ((flow-table flow-table))
    (= 0 (table-number flow-table)))


(ql:quickload :priority-queue)

(defclass packet ()
  ())

(defmethod sorted-flow-entries ((flow-table flow-table))
  (sort (remove-if-not #'(lambda (flow-entry)
			   (if (timeout? flow-entry)
			       (progn (send-remove-message-to-controller flow-entry)
				      t)))
		       (flow-entries flow-table))
	#'flow-entry-lessp))

(defmethod match ((flow-table flow-table) (packet packet) )
  (setf (flow-entries flow-table)
	(sorted-flow-entries flow-table))
  (find-if #'(lambda (entry)
	       (match entry packet)) 
	   (flow-entries flow-table)))

(defmethod flow-entry-lessp ((left flow-entry)  (right flow-entry) )
  (< (priority left)  (priority right)))

(defclass flow-entry ()
  ((match-fields :reader matched-fields)
   (counter :accessor counter)
   (priority :reader priority)
   (instructions :reader instruction)
   (idle-timeout :reader idle-timeout)
   (hard-timeout :reader hard-timeout)
   (match-time :reader match-time)
   (begin-time :reader begin-time)
   (cookie :reader cookie)
   (ofpff-send-flowrem? :reader ofpff-send-flowrem?)))

(defun current-time ()
  0
  ;todo
  )

(defmethod send-remove-message-to-controller ((flow-entry flow-entry))
  "switch must check the flow entry’s OFPFF_SEND_FLOW_REM flag. If
this flag is set, the switch must send a flow removed message to the
controller"
  ;; todo
  ;; message contain flow-entry, reason (:expiry :delete :eviction )
  ;; , duraction, statistics
  )


(defun is-timeout (current-time start timeout)
  (if (= 0 timeout)
      nil
      (> current-time (+ start timeout))))

(defmethod timeout? ((flow-entry flow-entry))
  (with-slots (match-time idle-timeout begin-time hard-timeout)  flow-entry
    (let* ( (current-time (current-time)))
      (or (is-timeout current-time match-time idle-timeout)
	  (is-timeout current-time begin-time hard-timeout)))))


;


(defmethod match ((flow-entry flow-entry) (packet packet))
  ;todo
  )



;;

(defclass openflow-port ()
  ()
  (:documentation "the network interfaces for passing packets between
  OpenFlow processing and the rest of the network")
  )
(defclass physical-port (openflow-port)
  ()
  (:documentation "correspond to a hardware interface of the switch"))
(defclass logical-port (openflow-port)
  ()
  (:documentation " a packet associated with a logical port may have
an extra metadata field called Tunnel-ID associated with it (see
7.2.2.7) and when a packet received on a logical port is sent to the
controller, both its logical port and its underlying physical port are
reported to the controller" ))

(defclass reserved-port (openflow-port)
  ()
  (:documentation "They specify generic forwarding actions such as
sending to the controller, flooding, or forwarding using non-OpenFlow
methods, such as “normal” switch processin"))

;; controller-port
(defclass controller-port (reserverd-port)
  ())
;; flooding-port
(defclass flooding-port (reserverd-port)
  ())
;; forwarding-port
(defclass forwarding-port (reserverd-port)
  ())


(defvar *all-port* (make-instance 'reserved-port)
  "be used only as an output port")
(defvar *controller-port* (make-instance 'reserved-port)
  "Represents the control channel with the OpenFlow controller. Can be
used as an ingress port or as an output port.")
(defvar *table-port* (make-instance 'reserved-port)
  "Represents the start of the OpenFlow pipeline (see 5.1). This port
is only valid in an output action in the action list of a packet-out
message (see 7.3.6), and submits the packet to the first flow table so
that the packet can be processed through the regular OpenFlow
pipeline")

(defvar *in-port* (make-instance 'reserved-port)
  "Represents the packet ingress port. Can be used only as an output port,
send the packet out through its ingress port.")

(defvar *any-port* (make-instance 'reserved-port)
  "Special value used in some OpenFlow commands when no port is specified (i.e.
port is wildcarded). Can neither be used as an ingress port nor as an output port.")
(defvar *local-port* (make-instance 'reserved-port)
  "Represents the switch’s local networking stack and its management stack.
Can be used as an ingress port or as an output port.")

(defvar *normal-port* (make-instance 'reserved-port)
  "Represents the traditional non-OpenFlow pipeline of the switch (see 5.1).
Can be used only as an output port and processes the packet using the
normal pipeline. If the switch cannot forward packets from the
OpenFlow pipeline to the normal pipeline, it must indicate that it
does not support this action.")

(defvar *flood-port* (make-instance 'reserved-port)
  "Represents flooding using the normal pipeline of the switch (see
5.1). Can be used only as an output port, in general will send the
packet out all standard ports, but not to the ingress port, nor ports
that are in OFPPS_BLOCKED state. The switch may also use the packet
VLAN ID to select which ports to flood.")



(defclass group-entry ()
  ((identifier :reader identifier :type (unsigned-byte 32))
   (group-type :reader group-type)
   (counters :reader counters)
   (action-buckets :reader action-buckets)))

(deftype group-type () '(member
			 :all ;; 
			 :select
			 :indirect
			 :fast-failover;; liveness mechanism 
			 ))

(defclass group-table ()
  (groups))


(defclass meta-table ()
  " Per-flow meters enable OpenFlow to implement various simple QoS
operations, such as rate-limiting, and can be combined with per-port
queues (see 5.12) to implement complex QoS frameworks, such as
DiffServ."
  
  (metas))

(defclass meta-entry ()
  (identifier :reader identifier :type (unsigned-byte 32))
  (counter :reader counters)
  (bands :reader bands))

(defclass meta-band ()
  ( (band-type :reader band-type :type band-type)
   (rate :reader rate)
  (counters :reader counters)
  (specific-arguments :reader specific-arguments)))
  
(deftype band-type ()
  '(member :drop :dscp-remark))
;; highest that lower than the current measured rate





(defclass openflow-pipeline ()
  ((flow-tables :reader flow-tables)))


(defclass tag ()
  ()
  ( :documentation "a header that can be inserted or removed from a
  packet via push and pop actions."))

(defclass meter ()
  ()
  (:documentation "a switch element that can measure and control the rate of packets"))


(defvar *pipeline* nil)









