(use-package :packet)

(defenum *ofp-flow-mod-command*
  ((:ofpfc-add 0 "new flow")
   (:ofpfc-modify 1 "modify all matching flows")
   (:ofpfc-modify-strict 2 "modify entry strictly matching wildcards and priority")
   (:ofpfc-delete 3 "delete all matching flows")
   (:ofpfc-delete-strict 4 "delete entry strictly matching wildcards and priority")))
  
; (enum :a *example-enum*)
(defenum *ofp-group-mod-command*
    ((:ofpgc-add 0 "new group")
     (:ofpgc-modify 1 "modify all matching groups")
     (:ofpgc-delete 2 "delete all matching groups")
     ))
;; for add request, if already resides in the group tables
;; the

(defun activate (request)
  (cond ((is-novalid (type request))
	 (send-ofp-error-msg :type :ofpet-group-mod-failed
			     :code :ofpgmc-invalid-group))
	((is-no-support-unequal-load request)
	 (send-ofp-error-msg :type :ofpet-group-mod-failed
			     :code :ofpgmc-weight-unsuppored))
	((is-lack-space request)
	 (send-ofp-error-msg :type :ofpet-group-mod-failed
			     :code :ofpgmc-out-of-buckets))
	
	((t 
	  (let* ((resides-in (in request *group-tables*)))
	
	(case (group-command request)
	  (:ofpgc-add (if resides-in
			  (send-ofp-error-msg :type :ofpet-group-mod-failed
					      :code :ofpgmfc-group-exists)))
	  (:ofpgc-modify (if resides-in
			     (do (remove-group request)
				 (add-group request))
			     (send-ofp-error-msg :;TODO: ype :ofpet-group-mod-failed
						 :code :ofpgmfc-unknown-group)))
	  (:ofpgc-delete (if resides-in nil
			     (remove-group request)))
	  )
	
	)))))
(defenum *ofp-meter-mod-command*
    ((:ofpgmc-add 0 "new meter")
     (:ofpgmc-modify 1 "modify specified meter")
     (:ofpgmc-delete 2 "delete specified meter")))

(defun send-meter-error (code )
  (send-ofp-error-msg :type :ofpet-meter-mod-failed
		      :code code ))
(defmethod activate(request)
  (case (meter-command request)
    (:ofpgmc-add   (if (is-in request *meter*)
		       (send-meter-error :ofpmmfc-meter-exists)
		       )
    (:ofpgmc-modify )
    (:ofpgmc-delete ))))


  

    
      
      
  

  

	  

  



;; flow table synchronisation


