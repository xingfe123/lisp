
(define-binary-struct (ip-address (:print-function print-ip-addr)) ()
  (value nil :binary-type ip-addr-octets))

(define-binary-struct (mac-address (:print-function print-mac-addr)) ()
  (value nil :binary-type mac-addr-octets))

(define-binary-struct ethh ()
  (dest nil :binary-type mac-addr)
  (src  nil :binary-type mac-addr)
  (type nil :binary-type u16)
  (:documentation "Ethernet frame header."))

(define-binary-struct arph ()
  (hardware-type   nil :binary-type u16)
  (protocol-type   nil :binary-type u16)
  (hardware-length nil :binary-type u8)
  (protocol-length nil :binary-type u8)
  (operation       nil :binary-type u16)
  (sender-ha       nil :binary-type mac-addr) ; ha = hardware address
  (sender-ip       nil :binary-type ip-addr)
  (target-ha       nil :binary-type mac-addr)
  (target-ip       nil :binary-type ip-addr)
  (:documentation "ARP frame header."))


(define-binary-bitfield-struct iph ()
  (version         nil   :binary-type ip-version)
  (hlen            nil   :binary-type ip-header-length)
  (tos             nil   :binary-type ip-type-of-service)
  (total-len       nil   :binary-type ip-total-length)
  (id              nil   :binary-type ip-identification)
  (flags           nil   :binary-type ip-flags)
  (fragment-offset nil   :binary-type ip-fragment-offset)
  (ttl             nil   :binary-type ip-time-to-live)
  (protocol        nil   :binary-type ip-protocol)
  (checksum        nil   :binary-type ip-header-checksum)
  (source          nil   :binary-type ip-addr)
  (dest            nil   :binary-type ip-addr)
  (options         '()))

(define-binary-struct icmph ()
  (type     0 :binary-type u8)
  (code     0 :binary-type u8)
  (checksum 0 :binary-type u16)
  (data     nil))

