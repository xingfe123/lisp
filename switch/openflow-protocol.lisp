
(defclass openflow-channel ()
  "connects each OpenFlow switch to a controller.  the controller
configures and manages the switch, receives events from the switch,
and sends packets out the switch"
  ;; should encrypted using tls, but may be run directly  over tcp 
  )

(defclass controller ()
  )


(defclass openflow-protocol()
  )


(deftype message-type ()
  '(member :controller-to-switch :asynchronous :symmetric))

;; features request.
;; must respond

;; configuration request.
;; only respond query

;; modify-state request
;; add/delete/modify flow/group entries in tables

;; read-state request
;;

;; 


;;;;; The open protocol

(defpacket ofp-header
    ((version (unsigned-byte 8) :documentation "ofp-version")
     (type (unsigned-byte 8) :documentation "one of the ofpt_ constants")
     (length (unsigned-byte 16) :documentation "length including this ofp-header")
     (xid    (unsigned-byte 32) :documentation "transaction id associated
     with this packet replies use the same id as was in the request to
     facilitate pairing"))
  (:documentation ""))

;; ox05
(defenum *ofp-type*
    ((:ofpt-hello )
     (:ofpt-error )
     (:ofpt-echo-request)
     (:ofpt-echo-reply)
     (:ofpt-experimenter)
     ;;  switch configuatio message
     (:ofpt-features-request)
     (:ofpt-features-reply)
     (:ofpt-get-config-request)
     (:ofpt-get-config-reply)
     (:ofpt-set-config)
     ;; asynchronous message
     (:ofpt-packet-in)
     (:ofpt-flow-remove)
     (:ofpt-port-status)
     ;; controller command messages
     (:ofpt-packet-out)
     (:ofpt-flow-mod)
     (:ofpt-group-mod)
     (:ofpt-port-mod)
     (:ofpt-table-mod)
     ;; multipart messages
     (:ofpt-multipart-request)
     (:ofpt-multipart-repy)
     ;; barrier messages
     (:ofpt-barrier-request)
     (:ofpt-barrier-reply)
     ;; controller role change request messages
     (:ofpt-role-request)
     (:ofpt-role-reply)
     ;; asynchronous message configuration
     (:ofpt-get-async-request)
     (:ofpt-get-async-reply)
     (:ofpt-set-async)
     ;; meter and rate limiters configuration messages
     (:ofpt-meter-mod)
     ;; controller role change event messages
     (:ofpt-role-status)
     ;; asynchronous message 
     (:ofpt-table-status)
     ;; request forwarding by the switch
     (:ofpt-requestforward)
     ;; bundle operation
     (:ofpt-bundle-control)
     (:ofpt-bundle-add-message)
     ))


;; sizeof(ofp-header) == 8









(defpacket ofp-port()
  ((port-no :u32 )
   (length :u16)
   (pad :u8 2)
   (hw-addr :u8 ofp-eth-alen)
   (pad2 :u8 2)
   (name :char ofp-max-port-name-len)
   (config :u32 )
   (state :u32)
   (properties :ofp-port-desc-prop-header)))
(defenum *port-no*
    max 0xffffff00
  in_port 8 
  table 9 
  normal fa 
  flood  fb 
  all     fc 
  controller fd
  local fe 
  any ff
  )

(defenum *ofp-port-config*
    port-down 1 << 0
  no-recv          2 
  no-fwd           5
  no-packet-in     6)

(defenum *ofp-port-state*
    link_down 0
  blocked 1
  live <<2)

(defenum *ofp-port-desc-prop-type*
    ethernet
  optical
  experimenter 
  )

(defpacket ofp-port-desc-prop-header()
  ((type :u16)
   (length :u16)))
(defpacket ofp-port-desc-prop-ethernet()
  ((:type u16)
   (:length u16)
   (pad :u8 4)
   (curr :u32)
   (advertised :u32)
   (supported :u32)
   (peer :u32)
   (curr-speed :u32)
   (max-speed :u32)))

(defenum *ofp-port-feature*
    10mb-hd
  10mb-fd
  100mb-hd


  copper << 11
  fiber << 12
  autoneg << 13
  pause  << 14
  pause-asyn <<15 
  )
  
;; sizeof(ofp-port) == 40





