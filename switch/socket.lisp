(ql:quickload :usocket)
(ql:quickload :usocket-server)
(ql:quickload :flexi-streams)
(ql:quickload :packet)

(defpackage :packet.server
  (:use :common-lisp :usocket :packet :flexi-streams))

(in-package :packet.server)

(defconstant +magic-number+ #x1E2B3A4D)

(defpacket person 
    ((name (:string 20) :initform "" :initarg :name :reader name)
     (counter :uint32 :initform 0 :initarg :counter :accessor counter))
  (:documentation "persion structure"))

(defpacket ip-address
    ((data (unsigned-byte 32) :initform 0 :accessor data))
  (:documentation "ip address"))

(defpacket mac-address
    ((data (unsigned-byte 48) :initform 0 :accessor data))
  (:documentation "mac address"))

(defpacket arp
    ((destination mac-address )
     (sender mac-address)
     (type :uint16 )
     (
     )
  (:documentation "arp packet"))


(defun packet-handler(buffer)
  ;;(socket-receive socket nil nil)
  ;; (declare (type (simple-array (unsigned-byte 8) *) buffer))
  (let* ((data (unpack buffer 'person)))
    (setf (counter data) (+ 1 (counter data)))
    (pack data 'person)))
    


(defun start-server(port)
  (format T "listen on ~a" port)
  (socket-server "127.0.0.1" port #'packet-handler nil
		 :protocol :datagram))
		 


(defun packet-send(port)
  (let* ((buffer (pack (make-instance 'person
				     :name "luoxing"
				     :counter 5)
		       'person))
	 (socket (usocket:socket-connect "127.0.0.1" port
					 :protocol :datagram)))
    (socket-send socket buffer (length buffer))
    (socket-receive socket buffer (length buffer))
    (with-slots (name counter) (unpack buffer 'person)
      (format nil "{name: ~a1~%, counter ~a}"
	      name 
	      counter))))


	 


		  

  

