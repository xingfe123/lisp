
(defclass openflow-channel()
  ((openflow-switch openflow-switch :reader openflow-switch :initform :controller)
   (openflow-controller openflow-controller :reader openflow-controller :initform :controller))
  (:documentation "the interface that connects each openflow switch to a controller")
  )

(defmethod send-hello()
  "each side of the connection must immediately send an ofpt_hello
  message with the version field set to the highest"
  )

(defmethod configure(channel)
  )

(defun manage(channel )
  )

(defun receive-event(channel)
  )
(defun send-packet(channel)
  )

;; all channel message must be formatted to the openflow protocol.
(defclass openflow-message()
  )
(defclass controller-to-switch-message(openflow-message)
  ()
  (:documentation "query feature, configure, modifiy-state,
  read-state, packet-out, barrier, role-request,
  asynchronous-configure"))

(defclass asynchronous-message(openflow-message)
  ()
  (:documentation "packet-in , "))
(defclass packet-in-message(openflow-message)
  ()
  (:documentation "packet-in "))
(defclass flow-removed-message(openflow-message)
  ()
  (:documentation "ofpff-send-flow-rem flag"))

(defclass port-status-message(openflow-message)
  ()
  (:documentation "port configuration or port state changes"))


(defclass symmetric-message(openflow-message)
  ()
  (:documentation "without solicitation, in either direction"))

(defclass hello-message(openflow-message)
  ()
  (:documentation "upon connection startup"))

(defclass echo-message(openflow-message)
  ()
  (:documentation "must return an echo replpy, They are mainly used to
  verify the liveness"))

(defclass error-message(openflow-message)
  ()
  (:documentation "indicate a faillure of q request initiated by the controller"))

(defclass experimenter-message(openflow-message)
  ()
  (:documentation "openflow switch to offer additional functionality
  within the openflow message type space"))


;; a switch may have one openflow channel to a single controller , or
;; multiple channels for reliability, each to a different controller







  

