(require 'usocket)

(defpackage :myclient
  (:use :common-lisp :usocket)
  (:nicknames :mycli)
  (:export "start-client"))

(in-package :myclient)

(defun start-client (ip port)
  (let ((sock (usocket:socket-connect ip port)))
    (progn
      (force-output (usocket:socket-stream sock))
      (do ((line                                                  
         (read-line (usocket:socket-stream sock) nil)             
         (read-line (usocket:socket-stream sock) nil)))
       ((not line))                                               
	(format t "~A" line)))))
