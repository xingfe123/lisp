; Strong leader

(defclass role)
;;leader,follower,candidate
(setq my-group (list
                [127 0 0 1 8080]
                [127 0 0 1 8090]
                [127 0 0 1 8100]))
(defclass raft-node()
  ((next-index :initarg :next-index :accesor next-index :initform 1)
   (match-index :initarg :match-index :accesor match-index :initform 0)
   (flags :initarg flags :flags :accesor flags :initform *raft-node-voting*)
   id))
(defun make-raft-node(data id)
  (make-instance 'raft-node
		 :udata data
		 :id id))

;; log index begin at 1
(defconstant +raft-node-voted-for-me+ 7)

(defun vote-for-me(node vote)
  (set-flags node +raft-node-voted-for-me+ vote))

(defun set-voting(node vote)
  (set-flags node +raft-node-voting+ vote)))

(defun set-sufficient-logs(node vote)
  (set-flags node +sufficient-logs+ vote))


(defun set-flags(node flags rf)
  (setf flags  (if vote
		   (bit-or rf flags)
		   (bit-and (bit-or rf flags) flags))))

(defun vote-for-me-p (node)
  (bit-and +raft-node-voted-for-me+ flags))

(defclass raft-server()
  )
(defun make-raft-server()
  (make-instance 'raft-server
		 :current-term 0
		 :voted-for -1
		 :timeout-elapsed 0
		 :election-timeout 1000
		 :request-timeout 200
		 :log (make-log)
		 :voting-cfg-change-log-index -1
		 :state :follower
		 :current-leader nil))

(defun raft-election-start(raft-server)
  (log raft-server "election starting: ~a ~a, term: ~a ci: ~a"
       election-timeout timeout-elapsed current-term)
  (raft-become-candiate raft-server))

(defun raft-become-candiate(raft-server)
  (log raft-server "becomming candidate")
  (incf (current-term raft-server)  1)
  ;; for (i = 0; i < me->num_nodes; i++)
  ;; 	 raft_node_vote_for_me(me->nodes[i], 0);
	 




  


(defclass log()
  ((size )
   (count)
   (front)
   (back)
   (base)
   (entries )
   ))


(defun ensure-capacity(log)
  (when (>= count size)
    (let ((tmp (make-array (* size 2) )))
      ;; for (i = 0, j = me->front; i < me->count; i++, j++)
    ;; {
    ;;     if (j == me->size)
    ;;         j = 0;
    ;;     memcpy(&temp[i], &me->entries[j], sizeof(raft_entry_t));
    ;; }
      )))

(defun make-log()
  (make-instance 'log
		 :size *initial-capacity*
		 :count 0
		 :back 0
		 :front 0
		 :entries (make-array *initial-capacity*)))

(defun clear-log(log)
  (count 0)
  (back front base))

(defun append-log(log entry)
  (do
   (ensure-capacity log)
   
    ;; if (me->cb && me->cb->log_offer)
    ;; {
    ;;     void* ud = raft_get_udata(me->raft);
    ;;     e = me->cb->log_offer(me->raft, ud, c, me->back);
    ;;     raft_offer_log(me->raft, c, me->back);
    ;;     if (e == RAFT_ERR_SHUTDOWN)
    ;;         return e;
    ;; }
   ; memcpy(&me->entries[me->back], c, sizeof(raft_entry_t));
   ))

(defun get-from-index(log index)
;;; assert (0 <= index -1)
  (when (base + count  >= index and index >= base)
    index -=1 ;; index starts at 1
    (let ((i (/ (+ front index base) size)))
    ;;    if (i < me->back)
    ;;     logs_till_end_of_log = me->back - i;
    ;; else
    ;;     logs_till_end_of_log = me->size - i;

      ;; *n_etys = logs_till_end_of_log;

      
  )

  
	  
	    

(defclass log-entry()
  ((index)
   (term)
   (command)))

(defvar raft-election-interval 0.5
  "Upper bound for time between elections, in seconds.")

(defun raft-election-duration ()
  (let ((i (/ (truncate (* 1000 raft-election-interval)) 2)))
    (/ (+ i (random i)) 1000.0)))

(defun raft-make-log-entry (index term command)
  (make-instance 'log-entry ))

;;;;;;;;

(defun start-node(port)
  (make-network-process  :port port)))

(defun start-cluster(&rest ports)
  (loop for port in ports
     do (make-network-process :port port)))





    
