

(ql:quickload :cl-redis)
(redis:with-connection (:host "localhost" :port 6379 )
  (red:select 1)
  (red:set "00" "xx")
  (red:get "00"))
(ql:quickload :iterate)
(ql:quickload :sqlite)
(sqlite:with-open-database (db "/home/luoxing/text")
  (sqlite:execute-non-query db
                            "insert into users (name, real) values (?, ?)"
                            "joe"
                            "Joe the User")
  (iterate:iter (for (id name real)
                     (execute-to-list db
                                      "select id, name, real from users where name = ?"
                                      "joe"))

                (collect (list id name real))))
