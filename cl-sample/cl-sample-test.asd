#|
  This file is a part of cl-sample project.
|#

(defsystem "cl-sample-test"
  :defsystem-depends-on ("prove-asdf")
  :author ""
  :license ""
  :depends-on ("cl-sample"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "cl-sample"))))
  :description "Test system for cl-sample"

  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
