#|
  This file is a part of cl-sample project.
|#

(defsystem "cl-sample"
  :version "0.1.0"
  :author "luoxing"
  :license "LLGPL"
;  :depends-on '(:clack)
  :components ((:module "src"
                :components
                ((:file "cl-sample"))))
  :description ""
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "cl-sample-test"))))
