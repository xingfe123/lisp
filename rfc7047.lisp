;;UUID A 128-bit identifier that is unique in space and time


;;JSON Usage
;; Null bytes (\u0000) SHOULD NOT be used in strings.
;; Only UTF-8 encoding is supported.

 ;; <database-schema>
 ;;      A JSON object with the following members:

 ;;           "name": <id>                            required
 ;;           "version": <version>                    required
 ;;           "cksum": <string>                       optional
 ;;           "tables": {<id>: <table-schema>, ...}   required



;; <table-schema>
;;     A JSON object with the following members:

;;        "columns": {<id>: <column-schema>, ...}   required
;;        "maxRows": <integer>                      optional
;;        "isRoot": <boolean>                       optional
;;        "indexes": [<column-set>*]                optional



;; <column-schema>
;; A JSON object with the following members:

;; "type": <type>                            required
;; "ephemeral": <boolean>                    optional
;; "mutable": <boolean>                      optional


;; <type>
;; The type of a database column.  Either an <atomic-type> or a JSON
;; object that describes the type of a database column, with the
;; following members:

;;  "key": <base-type>                 required
;; "value": <base-type>               optional
;; "min": <integer>                   optional
;; "max": <integer> or "unlimited"    optional


;; <base-type>
;; The type of a key or value in a database column.  Either an
;; <atomic-type> or a JSON object with the following members:

;; "type": <atomic-type>            required
;; "enum": <value>                  optional
;; "minInteger": <integer>          optional, integers only
;; "maxInteger": <integer>          optional, integers only
;; "minReal": <real>                optional, reals only
;; "maxReal": <real>                optional, reals only
;; "minLength": <integer>           optional, strings only
;; "maxLength": <integer>           optional, strings only
;; "refTable": <id>                 optional, UUIDs only
;; "refType": "strong" or "weak"    optional, only with "refTable"

