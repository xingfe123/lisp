;;;; test-project.lisp

(in-package #:test-project)

(defun hello()
  (format nil "h1x"))

(defclass foo()
  (bar))

(defun print-os-version()
  (format nil "int ~a, char ~a, pointer ~a"
		  (cffi:foreign-type-size :int)
		  (cffi:foreign-type-size :char)
		  (cffi:foreign-type-size :pointer)
		  (cffi:foreign-type-size :unsigned-int)
		  (cffi:foreign-type-size :long)
		  (cffi:foreign-type-size :unsigned-long)
		  (cffi:foreign-type-size :long-long)))

(cffi::defctype my-int :int)

										;(foreign-alloc :int )
(defvar tmp0 nil)
(defvar tmp1 nil)

(loop :for i :below 10 :do
   (setf (cffi:mem-aref tmp1 :int i) (* i 10)))

(defstruct tester0
  (a 1.0 :type single-float)
  (b 2 :type integer))

(cffi:defcstruct ftester0
  (a :float)
  (b :int)
  (c :int))


(cffi:defcstruct ftester0
  (a :int)
  (b :int)
  (c :int :offset 0)
  (c :int))

(cffi:defcenum (test-enum :int)
  (:monday )
  (:tuesday)
  (:wednesday))

(cffi:define-foreign-library lib-soil
  (:unix (:or "libSOIL.so"))
  (t (:default "libSOIL.so")))

(use-foreign-libray lib-soil)

  

(defclass person()
  ((name :initform "jeff")
   (age :initform 10)))

  




