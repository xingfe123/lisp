;;;; test-project.asd

(asdf:defsystem #:prove-example
  :serial t
  :description "Describe test-project here"
  :author "luo xing  <luoxing@gmail.com>"
  :license  "GPL"
  :version "0.0.1"
  :serial t
  :depends-on (:cffi :prove)
  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run) :prove) c))
  :components ((:file "prove-example")))
               
