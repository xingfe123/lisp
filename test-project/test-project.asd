;;;; test-project.asd

(asdf:defsystem #:test-project
  :serial t
  :description "Describe test-project here"
  :author "luo xing  <luoxing@gmail.com>"
  :license  "GPL"
  :version "0.0.1"
  :serial t
  :depends-on (:cffi)
  :components ((:file "package")
               (:file "test-project")))
