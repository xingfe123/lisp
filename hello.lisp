(in-package :common-lisp-user)
(defun hello()
  (write-string "hello, world!"))
;allow a running cl process to accept a Telent login on port 4000 on
										;the
(defun start-telnet (&optional (port 400))
  (let ((passive (socket:make-socket :connect :passive
									 :local-host "127.l"
									 :local-port port
									 :reuse-address t)))
	(mp:process-run-function
	 "telnet-listener"
	 #'(lambda (pass)
		 (let ((count 0))
		   (loop
				(let ((con (socket:accept-connection pass)))
				  (mp:process-run-function
				   (format nil "tel~d" (incf count))
				   #'(lambda (con)
					   (unwind-protect
							(tpl::start-interactive-top-level
							 con
							 #'tpl::top-level-read-eval-print-loop
							 nil)
						 (ignore-errors (close con :abort t)))))
				  con))))
	 passive)))


(defun our-member (obj lst)
  (cond ((null lst ) nil)
		((eql (first lst) obj) T)
		(T (our-member (rest lst)))))
(defun find-new-reaper(father-lst)
  (let ((pid-ns (task-active-pid-ns(father-lst))
				(thread father-lst))
		(loop (( father thread))
			  (if (and thread->flag pf_EXITING)
				  (recur (rest thread) )
				(if (unlikely (== father pid-ns->child-reaper))
					((setf pid-ns->child-reaper thread)  thread))
				
				(if (unlikely (== father pid-ns->child-reaper))
					(do (write-unlock-irq tasklist-lock)
						(if (unlikely (== pid-ns init-pid-ns))
							(panic "Attempted to kill init"))
					  (zap-pid-ns-processes pid-ns)
					  (write-lock-irq tasklist-lock)
					  (setf pid-ns->child-reaper init-pid-ns.child-reaper)))
				pid-ns->child-reaper)))))
(defun exit-ptrace (tracer)
  (let (())
	(write-lock-irq tasklist-lock)
	(do (if (__ptrace-detach tracer p)
			(list-add p->ptrace-entry ptrace-dead))
		(write-unlock-irq tasklist-lock)
	  (bug-on (not (list-empty tracer->ptraced)  ))
	  
		))
  )
