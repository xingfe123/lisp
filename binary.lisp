(in-package :common-lisp)
;;(ql:quickload :lisp-binary)
(ql:quickload :flexi-streams)
(ql:quickload :iterate)

(defpackage :binary-test
  (:use :common-lisp :ql :iterate))

(in-package binary-test)
;; ip address
(defbinary ip-address (:byte-order :big-endian)
  (data 0 :type 32))

(defmethod ip-address<((left ip-address) (right ip-address))
  (< (ip-address-data left)
     (ip-address-data right)))


(defun read-ip-address (stream &optional c n)
  (declare (ignore c n))
  (let* ((*readtable* (copy-readtable))
	 (vec (make-array '(4) :element-type '(integer 0 255))))
    (set-syntax-from-char #\. #\Space)
    (dotimes (i 4)
      (setf (elt vec i) (read stream t nil t)))
    (make-ip-address-from-arrary vec)))
  
(defun make-ip-address-from-arrary(vec)
  (read-binary 'ip-address (flexi-streams:make-in-memory-input-stream vec)))
  
(set-macro-character #\@ 'read-ip-address t)

(defun prefix-bits-to-netmask (n)
  "Return an IP-ADDR with the first N bits set to 1, and the rest 0.
  (PREFIX-BITS-TO-NETMASK 24) => @255.255.255.0"
  (declare (type (integer 0 32) n))
  (int-to-ip (lognot (1- (expt 2 (- 32 n))))))

;;; equalp
(defun int-to-ip(int-value)
  (make-ip-address :data int-value))

(defun ip-to-int(ip )
  (ip-address-data ip))

(defun int-to-octet-array(n size)
  (let ((array (make-array (list size) :initial-element NIL)))
    (iter (for i from 0 below size)
	  (after-each (setf (aref array (- size i 1))
			    (ldb (byte 8 (* 8 i)) n))))
    array))


(defun mask-ip (ip mask)
  (int-to-ip (logand (ip-to-int ip)
		     (ip-to-int mask))))
  

;; mac address tools
(defbinary mac-address (:byte-order :big-endian)
  (data 0 :type 48))

(defmethod ethernet-multicast-p ((mac-address mac-address))
  (= 1 (ldb (byte 1 40) (mac-address-data mac-address))))

(defun make-mac-address-from-array(array)
  (read-binary 'mac-address (flexi-streams:make-in-memory-input-stream array)))
  
(defun random-mac-address()
  (let ((data (random (expt 2 32))))
    (setf (ldb (byte 32 16) data) #x00FF)
    (make-mac-address :data data)))
;;; 
(defconstant other8 #e"01:02:03:04:05:06")

(defun mac-address-reader(stream &optional c n)
  (declare (ignore c n))
  (let* ((value-stream (make-string-input-stream (read stream t nil t)))
	 (*read-base* 16)
	 (*readtable* (copy-readtable))
	 (vec (make-array '(6) :element-type '(integer 0 255))))
    (set-syntax-from-char #\: #\Space)
    (dotimes (i 6)
      (setf (elt vec i) (read value-stream t nil t)))
    (read-binary 'mac-address (flexi-streams:make-in-memory-input-stream vec))))

(set-dispatch-macro-character #\# #\e #'mac-address-reader)


(defconstant broadcast-mac-address #e"FF:FF:FF:FF:FF:FF"
             "The ethernet broadcast address.")



;; ip addressbroadcast-mac-address


(define-enum ethernet-type 2 (:byte-order :big-endian)
	     (:arp #x0806)
	     (:rarp #x8035)
	     (:ip #x0800))


  

;;ARP 

(define-enum arp-operation 1 (:byte-order :dynamic)
	     (:request 1)
	     (:response 2))
;; IPv4

(define-enum ip-protocol 1 (:byte-order :dynamic)
	     (:icmp 1)
	     (:tcp 6)
	     (:udp 17))



    
;; IP options: control class

(defconstant ip-option-end-of-options        0)
(defconstant ip-option-nop                   1)
(defconstant ip-option-security              2)
(defconstant ip-option-loose-source-routing  3)
(defconstant ip-option-strict-source-routing 9)
(defconstant ip-option-record-route          7)
(defconstant ip-option-stream-id             4)
;; IP options: debugging class
(defconstant ip-option-timestamp 4)


;; ICMP
(define-enum icmp-type-echo 1
  (:reply 0)
  (:request 8))
  
  
(defconstant icmp-type-port-unreachable 3)

;; TCP
(defconstant tcpopt-end-of-options 0)
(defconstant tcpopt-nop            1)
(defconstant tcpopt-mss            2)


;;;(define-unsigned u48 6)

;;;(define-binary-vector ip-addrress-octets  u8 4)




(defbinary ethernet-header ()
  (dest 0 :type mac-address)
  (src 0 :type mac-address)
  (type  0 :type u16))

(defbinary arp-header()
  (hardware-type   nil :type u16)
  (protocol-type   nil :type u16)
  (hardware-length nil :type u8)
  (protocol-length nil :type u8)
  (operation       nil :type u16)
  (sender-mac       nil :type mac-address) ; ha = hardware address
  (sender-ip       nil :type ip-address)
  (target-ma      nil :type mac-address)
  (target-ip       nil :ype ip-address)
  (:documentation "ARP frame header."))

(defbinary ip-header()
  (version         nil   :type ip-version)
  (hlen            nil   :type ip-header-length)
  (tos             nil   :type ip-type-of-service)
  (total-len       nil   :type ip-total-length)
  (id              nil   :type ip-identification)
  (flags           nil   :type ip-flags)
  (fragment-offset nil   :type ip-fragment-offset)
  (ttl             nil   :type ip-time-to-live)
  (protocol        nil   :type ip-protocol)
  (checksum        nil   :binary-type ip-header-checksum)
  (source          nil   :binary-type ip-addr)
  (dest            nil   :binary-type ip-addr)
  (options         '()))


(defbinary icmp-header ()
  (type     0 :type u8)
  (code     0 :type u8)
  (checksum 0 :type u16))
  


(defbinary udp-header ()
  ;; IP addresses are inherited from IP layer
  (src-ip      :type ip-addrress)
  (dest-ip     :type ip-addrress)
  (src-port  nil :type u16)
  (dest-port nil :type u16)
  (length    nil :type u16)
  (checksum  0   :type u16))


(defbinary tcp-header ()
  ;; IP addresses are inherited from IP layer
  (src-ip      :type ip-address)
  (dest-ip     :type ip-address)
  (src-port    :type u16)
  (dest-port   :type u16)
  (seq         :type u32)
  (ack-seq     :type u32)
  (data-offset :type u4)
  (reserved    :type u4)
  (urg?        :type bitflag)
  (ack?        :type bitflag)
  (psh?        :type bitflag)
  (rst?        :type bitflag)
  (syn?        :type bitflag)
  (fin?        :type bitflag)
  (window      :type u16)
  (checksum    :type u16)
  (urgent-ptr  0   :type u16)
  (options     '())			; options (d)encoded by hand
  )

(defbinary tcp-pseudo-ip-header()
  (src-ip  nil :type ip-addr)
  (dest-ip nil :type ip-addr)
  (proto   :type u16)
  (len     :type u16))


;; Read syntax
;; looks like this:
;; @192.168.128.43
;; #e"F1:F2:f3:F4:f5:F6"

     
