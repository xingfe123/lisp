(ql:quickload :cl-ppcre)
(defun strip(str)
  (string-trim  '(#\Space #\Tab #\Newline) str))

(defun get-file(in-file out-file)
  (with-open-file (stream in-file)
    (with-open-file(out out-file :direction :output)
      (let* ((text (make-string (file-length stream))))
	(read-sequence text stream)
	(dolist (ip (map 'list #'strip (cl-ppcre:split "&" text)))
	  (format out "~a~%" ip))))))





    
