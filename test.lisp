(defpackage :tk.luoxing
  (:use :common-lisp :alexandria :iterate :cl-fad)
  (:export :bin-search
           :mirror?
           :tokens))

(in-package :tk.luoxing)

(defun bin-search(item vec)
  (labels ((finder (item vec start end)
             (if (= start end)
                 (if (eql item (aref vec start)) item)
                 (let* ((mid (floor (/ (+ start end) 2)))
                        (item1 (aref vec mid)))
                   (cond ((< item item1)
                          (finder item vec start (- mid 1)))
                         ((> item item1)
                          (finder item vec (+ mid 1) end))
                         (t item))))))
    (let ((len (length vec)))
      (when (not (zerop len))
        (finder item vec 0 (- len 1))))))
(defun mirror?(str)
  (let ((len (length str)))
    (if (evenp len)
        (do ((f 0 (+ f 1))
             (b (- len 1) (- b 1)))
            ((or (> f b)
                 (not (eql (elt str f) (elt str b))))
             (> f b))))))
(defun tokens(str  start)
  (labels ((constituent (c)
             (and (graphic-char-p c) (not (char= c #\space )))))
    (let ((p1 (position-if #'constituent str :start start)))
      (if p1
          (let((p2 (position-if (complement #'constituent)
                                str :start p1)))
            (cons (subseq str p1 p2)
                  (if p2
                      (tokens str p2))))))))
                      
    

  

;the most genreal output function in Common Lisp is format.

