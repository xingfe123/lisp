;;; Protocol stack overview.
;;
;; The name "protocol stack" suggests a set of protocols stacked on
;; top of one another. That's just how a TCP/IP stack is organised --
;; as layers of protocols, each building on the ones below. The
;; protocols currently supported are stacked like this:
;;
;; +------------------+
;; | ICMP | UDP | TCP |_____+
;; | IPv4             | ARP |
;; | Ethernet               |
;; | Physical network       |
;; +------------------------+

(defstruct interface()
	   (name :type string)
	   (type :type symbol)
	   (ip :type ip-address)
	   (netmask :type ip-address)
	   (broadcast-ip  :type ip-address)
	   (network-ip :type ip-address)
	   (bytes-in 0 :type integer)
	   (bytes-out 0 :type integer)
	   (up? nil :type (member nil t))
	   (mac :type mac-address)
	   (dev :type device))

(defmethod enable((interface interface))
  )
(defmethod disable((interface interface))
  )

(defmethod delete((interface interface))
  )

;; Physical layer
(defun network-input(iface frame)
  (declare (type simple-array frame))
  (incf (interface-bytes-in iface) (length frame))
  (let* ((*endian* :big-endian)
	 (*input-interface* iface)
	 (*ip-address* (interface-ip iface))
	 (*mac-address* (interface-mac iface)))
    (with-binary-input-from-vector (stream frame)
      (ethernet-input stream iface))))
	

(defun network-output(iface frame)
  )
	   


;;  Ethernet layer

(defun ethernet-input(stream frame)
  )

(defun ethernet-output(stream frame)
  )

;;; arp
(defvar *arp-cache* (make-hash-table :test #'equalp)
  "ARP cache, mapping IP addresses onto MAC addresses.")
(defun arp-input (stream iface)
  )

(defun arp-reply (req iface)
  )

;;; IPv4 (Routing)

(defvar *routes* ()
  "Routing table. Contains all routes, sorted from most- to least-
  specific network mask.")

(defstruct route()
	   (destination  :type ip-address)
	   (netmask      :type ip-address)
	   (gateway      :type ip-address)
	   (interface    :type interface))

(defun add-route (route)
  "Add a route to the routing table."
  (setq *routes*
        (merge 'list *routes* (list route) #'ip>= :key #'route-netmask)))

(defun lookup-route (ip)
  "Return the route to IP, or NIL if none is known."
  (find-if (lambda (route) (route-includes-p route ip))
           *routes*))

(defun route-includes-p (route ip)
  "Return true if ROUTE can be used to reach IP."
  (equalp (route-destination route)
          (mask-ip ip (route-netmask route))))



;;; IPv4 (Engine)
(defun ip-input (stream)
  "Process the IP packet contained in STREAM."
  (let ((ip-header (read-ip-header stream)))
    (if (ip-local-destination-p ip-header)
        (ip-local-input ip-header stream)
        #+nil (when *ip-allow-forwarding* (ip-forward iph stream)))))

(defun ip-local-input (ip-header stream)
  (case (ip-header-protocol ip-header)
    (#.ip-protocol-icmp (icmp-input iph stream))
    (#.ip-protocol-udp  (udp-input iph stream))
    (#.ip-protocol-tcp  (tcp-input iph stream))
    (otherwise
     (debug-print "Unrecognised IP protocol: #x~16,2,'0R"
                  (iph-protocol iph)))))


