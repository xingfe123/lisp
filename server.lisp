(require 'usocket)

(defpackage :myserver
  (:use :common-lisp :usocket)
  (:nicknames :mysrv)
  (:export "start-server"))

(in-package :mysrv)

(defun start-server1 (port)
  (socket-server "127.0.0.1"
		 port
		 #'(lambda (stream) (format
				     stream "~a~%" "hello usocket"))))

(defun process-s
