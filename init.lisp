
(defun quicksort(vee start size)
  (if (<= size 1) nil
      (let ((less-size (partition vee start size)))
	(quicksort vee (+ start less-size) (- size less-size))
	(quicksort vee start  (- less-size 1)))))
	

(defun partition(vee start size)
  (let ((less-size 0)
	(p (svref vee (+ start size -1))))
    (loop for i from start to (+ start size -1) do
	 (when (<= (svref vee i) p)
	   (rotatef (svref vee i) (svref vee (+ start less-size)))
	   (incf less-size)))
    less-size))

(defmacro ntimes(n &rest body)
  (let ((g (gensym)))
    `(loop for ,g from 0 to ,n do
	  ,@body)))

;; failed
;; (in 2 333 4 2 2)
(defmacro in (object &rest choices)
  (let ((insym (gensym)))
    `(let ((,insym ,object))
       (or ,@(mapcar #'(lambda (c) `(eql ,insym ,c))
		     choices)))))

(defmacro random-choice(&rest exprs)
  `(case (random ,(length exprs))
     ,@(let ((key -1))
	    (mapcar #'(lambda (expr) `(,(incf key) ,expr))
		    exprs))))

(defmacro with-gensyms(syms &body body)
  `(let ,(mapcar #'(lambda (s) `(,s (gensym)))
		 syms)
     ,@body))

(defmacro avg(&rest args)
  `(/ (+ ,@args) ,(length args)))

(defmacro aif(test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))
		 



		     



(defclass rectangle ()
  (height width))

(defclass circle()
  (radius))

(defclass colored()
  (color))

(defclass colored-circle(circle colored)
  ())

(defmethod area((x rectangle))
  (* (slot-value x 'height)
     (slot-value x 'width)))


(defmethod area((x circle))
  
