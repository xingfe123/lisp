(defun parse-log(file-name)
  (with-open-file (in file-name :direction :input)
    (loop for text = (read-line in nil nil) while text
       for entry = (handler-case (parse-log-entry text)
		     (malformed-log-entry-error () nil))
       when entry collect it)))
(define-condition malformed-log-entry-error (error)
  (text :initarg :text :reader text))


(defun parse-log-entry (text)
  )
		     
