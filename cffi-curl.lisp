(ql:quickload :cffi)

(defpackage :cl-libcurl
  (:use :common-lisp :cffi))
(in-package :cl-libcurl)

(define-foreign-library libcurl
    (:darwin (:or "libcurl.3.dylib" "libcurl.dylib"))
    (:unix (:or "libcurl.so.3" "libcurl.so"))
    (t (:default "libcurl")))

(use-foreign-library libcurl)

(defctype curl-code :int)

(defcfun "curl_global_init" curl-code
  (flags :long))

(defcfun "curl_easy_init" :pointer)

(defcfun "curl_easy_cleanup" :void
  (easy-handle :pointer))







