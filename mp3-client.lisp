(defclass playlist()
  ((id :accessor id :initarg :id)
   (songs-table :accessor songs-table :initform (make-playlist-table))
   (current-song :accessor current-song :initform *empty-playlist-song*)
   (current-index :accessor current-index :initform 0)
   (ordering :accessor ordering :initform :album)
   (shuffle :accessor shuffle :initform :none)
   (repeat :accessor repeat :initform :none)
   (user-agent :accessor user-agent :initform "Unkown")
   (lock :reader lock :initform (make-process-lock))))

(defun make-playlist-table()
  (make-instance 'table :schema *mp3-schema*))
(defvar *playlists* (make-hash-table :test #'equal))
(defparameter *playlists-lock* (make-process-lock :name "playlists-lock"))
(defun lookup-playlist(id)
  (with-process-lock (*playlists-lock*)
    (or (gethash id *playlists*)
	(setf (gethash id *playlists*)
	      (make-instance 'playlist :id id)))))

  
		
